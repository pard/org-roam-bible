#!/usr/bin/env python3

import io
import pathlib
import requests
import zipfile


def download(url):
    content = requests.get(url).content
    return unzip(content)


def unzip(f):
    return zipfile.ZipFile(io.BytesIO(f))


def write_file(path, content):
    with open(path, "w") as f:
        f.write(content)


def create_headers(title, aliases, headline):
    return [
        f"#+title: {title.strip().replace('.', '')}",
        f"#+roam-alias: {aliases}",
        f"* {headline.strip()}",
    ]


def enum(lines):
    enumerated = []
    for count, line in enumerate(lines, 1):
        enumerated.append(f"{count}. {line.strip()}")
    return enumerated


def main():
    src_url = "https://ebible.org/Scriptures/engwebp_readaloud.zip"
    zipped_src = download(src_url)
    parent_dir = "bible/web/"
    for f in zipped_src.namelist():
        parts = f.split("_")
        if "engwebp" in parts[0] and "000" not in parts[1]:
            book_abbr = parts[2]
            book_dir = parent_dir + book_abbr
            chapter = parts[3]
            pathlib.Path(book_dir).mkdir(parents=True, exist_ok=True)
            path = f"{book_dir}/{chapter}.org"
            body = [x.decode("utf-8-sig") for x in zipped_src.open(f).readlines()]
            header, aliases, headline = create_headers(body[0], book_abbr, body[1])
            content = "\n".join([header, aliases, "", headline] + enum(body[2:]))
            write_file(path, content)


if __name__ == "__main__":
    main()
